import pandas as pd

def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    def extract_title(name):
        if 'Mr.' in name:
            return 'Mr.'
        elif 'Mrs.' in name:
            return 'Mrs.'
        elif 'Miss.' in name:
            return 'Miss.'
        else:
            return None

    df['Title'] = df['Name'].apply(extract_title)
    grouped_df = df.groupby('Title').mean()
    missAvg = grouped_df.loc['Miss.', 'Age'].round().astype(int)
    mrAvg = grouped_df.loc['Mr.', 'Age'].round().astype(int)
    mrsAvg = grouped_df.loc['Mrs.', 'Age'].round().astype(int)

    null_counts = df['Age'].isna().groupby(df['Title']).sum()
    null_counts_list = null_counts.tolist()

    avg_map = {'Miss.': missAvg, 'Mr.': mrAvg, 'Mrs.': mrsAvg}
    df['Age'] = df['Age'].fillna(df['Title'].map(avg_map))

    result = list(zip(['Mr.', 'Mrs.', 'Miss.'], null_counts_list, [missAvg, mrAvg, mrsAvg]))
    return result

